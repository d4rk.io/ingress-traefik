#!/bin/env bash
set -e

if [ -f .env ]; then
  source .env
fi

if [ ! -d /srv/var/lib/ingress-traefik-r1-${UUID}/ ]; then
  sudo mkdir /srv/var/lib/ingress-traefik-r1-${UUID}/
fi

if [ ! -f /srv/var/lib/ingress-traefik-r1-${UUID}/acme.json ]; then
  sudo touch /srv/var/lib/ingress-traefik-r1-${UUID}/acme.json
  sudo chmod 600 /srv/var/lib/ingress-traefik-r1-${UUID}/acme.json
fi
